import regexps from './regexps-config.js'
import toJson from './utils/paramsToJson'

export default directiveTockenizer

function directiveTockenizer(tokeniser, variant, eat, value, silent){

	let matchToken =  matchFinder(variant,value)

	if(!matchToken) return

	if (silent) return true;


	const directive = matchToken.directive()
  const {props} = matchToken.selectedRegexp
  const { full : fullMatch, before, after} = matchToken.composed()

  //If there is a before string, return it with no processing
  if( before != ''){
    return 
  }

	let node = {
	    type: 'directive',
	    variant: props.variant,
	    specie: props.specie,
	    data: {directive: directive},
	}


	//specifics children processing with "species" and "variants"

	//in any case, we tokenise the directive's body
	node.children = tokeniser.tokenizeInline(directive.body, eat.now())
	//for container, process the block inside
	if(props.variant === 'block' && props.specie === 'container'){
		node.children = tokeniser.tokenizeBlock(directive.content, eat.now())
  }

	return eat(fullMatch)(node)
}


// return a matchTocken object with props ////@TODO : use documentation.js////
function matchFinder(variant,value){
	const f = regexps.filter(r => r.props.variant === variant)

    // this return the matching that occur in first position
    
    const matchTokenList = []

    f.forEach(regexpConf => {
      //rs for rsearch result
      const rs = rsearch(value,regexpConf.query)
      if(rs){
        matchTokenList.push(createMatchToken(regexpConf,rs,value))
      }
    }
   )
   matchTokenList.sort((a, b)=> a.startIndex - b.startIndex)
  
   // return the first match that occur
   if(matchTokenList[0]) {
     return matchTokenList[0]
    }

   return 
}


function rsearch(value,regexp){
  const re = new GroupRegExp(regexp);
	return value.match(re);
}

function createMatchToken(regexpConf, rs, value){

  const fullMatch = rs.fullMatch()
  const startIndex = value.indexOf(fullMatch)
  const before = value.slice(0,startIndex)

  const endIndex = startIndex + fullMatch.length 
  const after = value.slice(endIndex,value.length)

  const matchToken = {
    searchResult : rs,
    selectedRegexp : regexpConf,
    startIndex : startIndex,
    directive(){
      return rs.values()
    },
    composed(){
      return {
        before : before,
        // here match is equivalent to directive 
        match  : rs.values(),
        after  : after,
        full   : fullMatch,
        matchStartIndex : startIndex,
        matchEndIndex   : endIndex,
      }
    }
  }

  return matchToken
}


// source : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/@@match
class GroupRegExp extends RegExp {
  [Symbol.match](str) {
    var result = RegExp.prototype[Symbol.match].call(this, str);
    if (!result) return null;
    return {
      group(n) {
        return result[n];
      },
      values(){
        const directiveName_INDEX = 1
        const directiveBody_INDEX = 2
        const directiveParams_INDEX = 3
        // index 4 is params values with no '{' around
        const directiveContent_INDEX = 5
        const parsed = {}
        
        if(result[directiveName_INDEX]){ parsed.name = result[directiveName_INDEX]}
        if(result[directiveBody_INDEX]){ parsed.body = result[directiveBody_INDEX]}
        if(result[directiveParams_INDEX]){ parsed.params = toJson(result[directiveParams_INDEX])}
        if(result[directiveContent_INDEX]){ parsed.content = result[directiveContent_INDEX]}

        return parsed
      },
      fullMatch(){
            return result[0]
      }
    };
  }
}

