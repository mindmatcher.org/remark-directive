import directiveTockenizer from './directive-tokenizer'

export default blockTokenizer

function blockTokenizer (eat, value, silent) {
	return directiveTockenizer(this,'block',eat,value,silent)
}