import regexps from './regexps-config.js'
import directiveTockenizer from './directive-tokenizer'

export default inlineTokenizer


// Inline Tokenizer
function inlineTokenizer (eat, value, silent) {
	return directiveTockenizer(this,'inline',eat,value,silent)
}

inlineTokenizer.locator = locator

function locator (value, fromIndex) {
	const f = regexps.filter(r => r.props.variant === 'inline')
	let r = -1
	
	f.forEach(regexpConf => {
	  const t = regexIndexOf(value,regexpConf.query,fromIndex)
	  r = t >= 0 ? t : r
	})

	if(r >= 0 ) { 
		return r 
	}

	return r
}

function regexIndexOf(text, re, i) {
	const indexInSuffix = text.slice(i).search(re);
	return indexInSuffix < 0 ? indexInSuffix : indexInSuffix + i;
}
