

const regexps = [
    {
      title:'Inline Directive Regexp',
      query: /(?<!:)[:]{1}([^: ]*)\[(.*?)]({(.*?)})?/,
      props: {
        type: 'directive',
        variant: 'inline',
        specie: 'inline',
      }
    },
    {
      title:'Leaf Block Directive Regexp',
      query: /(?<!:)[:]{2}([^: ]*)\[(.*?)]({(.*?)})?/,
      props: {
        type: 'directive',
        variant: 'block',
        specie: 'leaf',
      }
    },
    {
      title:'Container Block Directive Regexp',
      query: /(?<!:)[:]{3}([^: ]*)\[(.*?)]({(.*?)})?(\n.*?):::/s,
      props: {
        type: 'directive',
        variant: 'block',
        specie: 'container',
      }
    }

  ]

export default regexps

//warning : params_start & params_end are not really configurable until it's only a Json.stringify
export const delimiters = {
                            directive    : ':',
                            body_start   : '[',
                            body_end     : ']',
                            params_start : '{',
                            params_end   : '}'
                          }