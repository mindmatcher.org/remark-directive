
export default (paramString) =>{
    if(!paramString) return 
    
    try{
        return JSON.parse(paramString)
    }catch(err){
        console.warn('error in processing param. Return it as is.')
        console.log(paramString)
        console.log(err)
        return paramString
    }
}