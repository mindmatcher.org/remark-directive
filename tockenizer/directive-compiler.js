import { delimiters as delims}  from './regexps-config.js'

export default (node) =>{

    //TODO ? put delimiters directly inside regexps-config and use delims.xx
    //dds for directive delimiter start
    let dds = ''
    if (node.specie === 'inline'){ dds = ':'}
    if (node.specie === 'leaf'){ dds = '::'}
    if (node.specie === 'container'){ dds = ':::'}

    //dde for directive delimiter end
    let dde = node.specie === 'container' ? ':::' : ''

    const {name, body, params, content} = node.data.directive
    let n = name ? name : ''
    let b = body ? `${delims.body_start}${body}${delims.body_end}` : ''
    let p = params ? `${paramsToString(params)}` : ''

    //warning : who to "pass the compilation" of content to another compiler ? 
    let c = content ? content : ''

    const r = `${dds}${n}${b}${p}${c}${dde}`

    return `${r}`
}

function paramsToString( params){
    if( typeof params === 'string') return params
    if( typeof params === 'object') return JSON.stringify(params)
    return params
}