'use strict'

import fs from 'fs'
import path from 'path'
import test from 'tape'
import vfile from 'to-vfile'
import remark from 'remark'
import not from 'not'
import hidden from 'is-hidden'
import frontmatter from  '..'

import inspect from 'unist-util-inspect'
import directives from '../.'
let testFile = './remark-directives/examples/directive-X.md'
import stringify from 'remark-stringify'

//inspired by : 
// https://github.com/remarkjs/remark-frontmatter/blob/master/test/index.js
// example of folder : https://github.com/remarkjs/remark-frontmatter/tree/master/test/fixtures/custom-default



var join = path.join
var read = fs.readFileSync
var write = fs.writeFileSync
var dir = fs.readdirSync


test('fixtures', function(t) {
  var base = join(__dirname, 'fixtures')
  var entries = dir(base).filter(not(hidden))

  t.plan(entries.length)

  entries.forEach(each)

  function each(fixture) {
    t.test(fixture, function(st) {
      var input = vfile.readSync(join(base, fixture, 'input.md'))
      var treePath = join(base, fixture, 'tree.json')
      var outputPath = join(base, fixture, 'output.md')
      var output
      var actual
      var expected
      var config
      var proc

      try {
        config = JSON.parse(read(join(base, fixture, 'config.json')))
      } catch (error) {}

      proc = remark().use(directives, config)
      // proc = remark().use(directives, config).use(stringify, {
      //   bullet: '*'
      // })

      actual = proc.parse(input)

      try {
        output = read(outputPath, 'utf8')
      } catch (error) {
        output = String(input)
      }

      try {
        expected = JSON.parse(read(treePath))
      } catch (error) {
        // New fixture.
        write(treePath, JSON.stringify(actual, 0, 2) + '\n')
        expected = actual
      }
      
      st.deepEqual(actual, expected, 'tree')
      st.equal(String(proc.processSync(input)), output, 'process')
      st.end()
    })
  }
})

