---
vocab:
    http://schema.org/
mappings:
    program:
        OfferCatalog: name
    p2:
        defaultParams: { day:auto sub:'ListItem:itemListElement' itemType:CreativeWork }
    cname:
        CreativeWork: name
---

* here comes a test **with** strong
* You can get informations about our :name[Juriste & Numérique] online training and our :org[orgName]{ with:parameters} here.


* ::gdocs[You can view here an iframe of one of our training content]{ docId=1234 }


:::program[here commes the programName]{ day=1 sub=ListItem:itemListElement itemType=CreativeWork }
* First Item
* Second Item
:::

