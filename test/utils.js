import escodegen from 'escodegen'
import toU from 'unist-builder-blueprint'

// https://github.com/syntax-tree/unist-builder-blueprint
var tree = {
  type: 'root',
  children: [
    {type: 'subtree', id: 1},
    {
      type: 'subtree',
      id: 2,
      children: [
        {
          type: 'node',
          children: [
            {type: 'leaf', value: 'leaf 1'},
            {type: 'leaf', value: 'leaf 2'}
          ]
        },
        {type: 'leaf', id: 3, value: 'leaf 3'},
        {type: 'void', id: 4}
      ]
    }
  ]
}

var estree = toU(tree)
console.log('***')
console.log(estree)
console.log('***')
console.log(escodegen.generate(estree, {format: {indent: {style: '  '}}}))


console.log('/////////////: second test')

// https://github.com/syntax-tree/unist-builder
import u from 'unist-builder'

var tree = u('root', [
  u('subtree', {id: 1}),
  u('subtree', {id: 2}, [
    u('node', [u('leaf', 'leaf 1'), u('leaf', 'leaf 2')]),
    u('leaf', {id: 3}, 'leaf 3'),
    u('void', {id: 4})
  ])
])

console.dir(tree, {depth: null})

console.log('etape 3 A FAIRE ')
console.log('https://github.com/syntax-tree/unist-util-filter/blob/master/test.js#L7')

