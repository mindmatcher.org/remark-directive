

## Warnings 

* This is not actually pushed on npm, so you have to get a local clone of the file and adapt the import path
* The "compiler" is actually totally dummy and don't well compile. this is a work in progress.
* Theses regex will not work on Firefox as of 20191128 [negative lookbehind](https://bugzilla.mozilla.org/show_bug.cgi?id=1225665) is not implemented
* This directive implementation is based on a [proposal by wooorm](https://github.com/micromark/common-markup-state-machine/issues/4#issuecomment-542584899) and is not part of the micromark specs (for now, let's hope)

* For the 2 block types a new line have to be present before the content to be processed. 
  * A review have to be done on the block->leaf directive to define if it's a bloc or an inline type

## Install dependencies

```
npm install remark to-vfile unist-util-inspect
```

## Use 



* In the next example change the testFile value to suit your need

* Example in a remark flow : 
``` js
import remark from 'remark'
import vfile from 'to-vfile'
import inspect from 'unist-util-inspect'
import directives from './remark-directives'
//you can find file examples here : './remark-directives/test/fixtures/*.md'
const testFile = '/YOUR/TESTFILE.md'

remark()
  .use(directives)
  .use(astViewer)
  .process(vfile.readSync(testFile), logFile)


function astViewer() {
  return transformer
  function transformer(tree) {
    console.log('*******')
    console.log('AST inspector :')
    console.log(inspect(tree))
    console.log('*******')
  }
}

function logFile(err, file) {
  if (err) throw err

  console.log('==========')
  console.log(file)
  console.log('==========')
}
```

## Directives definitions 

### Directive { type : 'inline', specie : 'inline'}
* Syntax 
```
:name[body]{params}
```

* with `directive-1.md`, yield : 
```
root[1] (1:1-3:1, 0-127)
└─ list[1] (2:1-2:126, 1-126) [ordered=false][spread=false]
   └─ listItem[1] (2:1-2:126, 1-126) [spread=false]
      └─ paragraph[5] (2:3-2:126, 3-126)
         ├─ text: "You can get informations about our" (2:3-2:37, 3-37)
         ├─ directive[1] (2:37-2:64, 37-64) [variant="inline"][specie="inline"][data={"directive":{"name":"name","body":"Juriste & Numérique"}}]
         │  └─ text: "Juriste & Numérique" (2:37-2:56, 37-56)
         ├─ text: " online training and our" (2:64-2:88, 64-88)
         ├─ directive[1] (2:88-2:120, 88-120) [variant="inline"][specie="inline"][data={"directive":{"name":"org","body":"orgName","params":"{ with:parameters}"}}]
         │  └─ text: "orgName" (2:88-2:95, 88-95)
         └─ text: " here." (2:120-2:126, 120-126)
```

### Directive { type : 'block', specie : 'leaf'}
* Syntax 
```
::name[body]{params}
```

* with `directive-2.md`, yield : 
```
root[1] (1:1-3:1, 0-64)
└─ list[1] (2:1-2:63, 1-63) [ordered=false][spread=false]
   └─ listItem[1] (2:1-2:63, 1-63) [spread=false]
      └─ directive[1] (2:3-2:63, 3-63) [variant="block"][specie="leaf"][data={"directive":{"name":"gdocs","body":"A directive for an Iframe for example","params":"{ docId=1234 }"}}]
         └─ text: "A directive for an Iframe for example" (2:3-2:40, 3-40)
```

### Directive { type : 'block', specie : 'container'}
* Syntax 
```
:::name[body]{params}
content
:::
```

* with `directive-3.md`, yield : 
```
root[1] (1:1-6:1, 0-132)
└─ directive[1] (2:1-6:1, 1-132) [variant="block"][specie="container"][data={"directive":{"name":"program","body":"here commes the programName","params":"{ day=1 sub=ListItem:itemListElement itemType=CreativeWork }","content":"\n* First Item\n* Second Item\n"}}]
   └─ list[2] (3:1-4:14, 101-127) [ordered=false][spread=false]
      ├─ listItem[1] (3:1-3:13, 101-113) [spread=false]
      │  └─ paragraph[1] (3:3-3:13, 103-113)
      │     └─ text: "First Item" (3:3-3:13, 103-113)
      └─ listItem[1] (4:1-4:14, 114-127) [spread=false]
         └─ paragraph[1] (4:3-4:14, 116-127)
            └─ text: "Second Item" (4:3-4:14, 116-127)
```

## tests 

`̀``
node -r esm test/
`̀``

* side note : for embeded in a project in a docker
```
docker-compose exec resem bash
# inside the container : 
cd src/sharedlibs/remark-directive
node -r esm test/
```

## TODOS : 

* Allow mdhast transformation for directive in header (example : `# :label[]{"@lang":"en"}`). This actually throw an error.


## Licence : Apache 2.0