import inlineTokenizer from './tockenizer/inlineTokenizer'
import blockTokenizer  from './tockenizer/blockTokenizer'
import directiveCompiler from './tockenizer/directive-compiler'

export default directivesPlugin


//TODO : pass parameters like previousInlineMethod, previousBlockMethod, and showMethodInsert (debug)
function directivesPlugin () {

  const showMethodInsert = false
  
  //dmn for directiveMethodName
  let dmn = 'directive'

  const Parser = this.Parser

  // Inject the inlineTokeniser after `previousInlineMethod`
  // list and order of default inlinetokenisers : https://github.com/remarkjs/remark/tree/master/packages/remark-parse#parserinlinetokenizers  
  const inlineTokenizers = Parser.prototype.inlineTokenizers
  const inlineMethods = Parser.prototype.inlineMethods
  inlineTokenizers[dmn] = inlineTokenizer

  let previousInlineMethod = 'break'
  insertAfter(dmn, previousInlineMethod, inlineMethods, showMethodInsert)

  // Inject a blockTokeniser, after `previousBlockMethod`
  // list and order of default blockTokeniser : https://github.com/remarkjs/remark/tree/master/packages/remark-parse#parserblockmethods
  // source : https://github.com/zestedesavoir/zmarkdown/blob/master/packages/remark-iframes/src/index.js
  const blockTokenizers = Parser.prototype.blockTokenizers
  const blockMethods = Parser.prototype.blockMethods
  blockTokenizers[dmn] = blockTokenizer
  
  let previousBlockMethod = 'table'
  insertAfter(dmn, previousBlockMethod, blockMethods, showMethodInsert)
  

  //compiler definition for node stingifycation (@TODO : check that)
  const Compiler = this.Compiler
  if (Compiler) {
    const visitors = Compiler.prototype.visitors
    if (!visitors) return
    visitors.directive = directiveCompiler
  }

}


function insertAfter(newMethodName, methodName, methodsList,showMethodInsert = false){
  const afterIndex = methodsList.indexOf(methodName) + 1
  if(showMethodInsert){ 
    console.log(`Insert method '${newMethodName}' into :`)
    console.log(methodsList)
  }

  methodsList.splice(afterIndex, 0, newMethodName)

  if(showMethodInsert){ 
    console.log(`Insert method '${newMethodName}'. After insertion :`)
    console.log(methodsList)
  }
  
}